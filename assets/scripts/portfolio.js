const portfolioBtn = menu.lastElementChild.firstElementChild;

portfolioBtn.addEventListener("click", function () {
  event.preventDefault();

  aboutmePage.style.display = "none";
  resumePage.style.display = "none";
  contactPage.style.display = "none";
  portfolioPage.style.display = "flex";
});

const portfolioBtnForPhone =
  menu.previousElementSibling.firstElementChild.firstElementChild
    .nextElementSibling;
console.log(portfolioBtnForPhone);

portfolioBtnForPhone.addEventListener("click", portfolio);

function portfolio() {
  event.preventDefault();

  aboutmePage.style.display = "none";
  resumePage.style.display = "none";
  portfolioPage.style.display = "flex";
  contactPage.style.display = "none";
}
