const resumeBtn = menu.firstElementChild.lastElementChild;

console.log(resumeBtn);

resumeBtn.addEventListener("click", function () {
  event.preventDefault();

  aboutmePage.style.display = "none";
  portfolioPage.style.display = "none";
  contactPage.style.display = "none";
  resumePage.style.display = "flex";
});

const resumeBtnForPhone =
  menu.previousElementSibling.firstElementChild.firstElementChild
    .nextElementSibling.nextElementSibling;
console.log(resumeBtnForPhone);

resumeBtnForPhone.addEventListener("click", resume);

function resume() {
  event.preventDefault();

  aboutmePage.style.display = "none";
  resumePage.style.display = "flex";
  portfolioPage.style.display = "none";
  contactPage.style.display = "none";
}
